# The official repository for the Rock the JVM Akka HTTP with Scala course (Udemy edition)

This repository contains the code we write
during  [Rock the JVM's Akka HTTP with Scala](https://www.udemy.com/akka-http) course on Udemy. 