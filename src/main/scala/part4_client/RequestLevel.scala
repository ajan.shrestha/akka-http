package part4_client

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, Uri}
import akka.stream.scaladsl.Source
import spray.json._

import java.util.UUID
import scala.util.{Failure, Success}

object RequestLevel extends App with PaymentJsonProtocol {
  /*
   * request-level API
   *
   *  Benefits
   *    - the freedom from managing anything
   *    - dead simple
   *
   *  Best for:
   *    - low-volume, low-latency requests
   *  Do not use the request-level API for:
   *    - high volume (use the host-level API)
   *    - long-lived requests (use the connection-level API)
   */

  implicit private val system: ActorSystem = ActorSystem("RequestLevel")

  import system.dispatcher

  private val responseFuture = Http().singleRequest(HttpRequest(uri = "https://www.google.com"))
  responseFuture.onComplete {
    case Success(response) =>
      // VERY IMPORTANT
      response.discardEntityBytes()
      println(s"The request was successful aad returned: $response")
    case Failure(ex) =>
      println(s"The request failed with: $ex")
  }

  import PaymentSystemDomain._

  private val creditCards = List(
    CreditCard("4242-4242-4242-4242", "424", "tx-test-account"),
    CreditCard("1234-1234-1234-1234", "123", "tx-daniel-account"),
    CreditCard("1234-1234-4321-4321", "321", "my-awesome-account"),
  )

  private val paymentRequests = creditCards.map(creditCard =>
    PaymentRequest(creditCard, "rtjvm-store-account", 99))
  private val serverHttpRequests = paymentRequests.map(paymentRequest =>
    HttpRequest(
      HttpMethods.POST,
      uri = Uri("http://localhost:8080/api/payments"),
      entity = HttpEntity(
        ContentTypes.`application/json`,
        paymentRequest.toJson.prettyPrint
      )
    )
  )

  Source(serverHttpRequests)
    //.mapAsync(10)(request => Http().singleRequest(request)) // ordered
    .mapAsyncUnordered(10)(request => Http().singleRequest(request)) // unordered
    .runForeach(println)
}
