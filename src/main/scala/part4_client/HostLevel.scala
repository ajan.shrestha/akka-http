package part4_client

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, HttpResponse, StatusCodes, Uri}
import akka.stream.scaladsl.{Flow, Sink, Source}
import spray.json._

import java.util.UUID
import scala.util.{Failure, Success, Try}

object HostLevel extends App with PaymentJsonProtocol {
  /*
   * Goal
   *  Host-level API
   *    - provides a connection pool to a given host and port combinations
   *      so that we don't have to manage connections individually
   *    - individual requests can reuse those connections transparently
   *    - API is recommended for high volume and low latency requests.
   *
   *  Benefits
   *    - the freedom from managing individual connections
   *    - the ability to attach data to requests (aside from payloads)
   *
   *  Anti-pattern
   *    - send a one-off requests through Host-level API (use the request-level API)
   *    - long-lived requests( use the connection-level API)
   *
   *  Usage
   *    - High volume and low latency requests
   */

  implicit private val system: ActorSystem = ActorSystem("HostLevel")

  import system.dispatcher

  /*
   * Reason for the extended type
   *  - we don't manage individual connections,
   *    the connections are stored in a cached pool and reused
   *  - this means that the requests that is sent through the pool will go through
   *    any of the available connections and as such the HTTP responses may be in a
   *    different order than the original order of requests.
   *  - this API allows us to make an association between a request and a value
   *  - when we receive a response, we gain the same value attached to the response
   *    associated with the original request.
   */
  private val poolFlow: Flow[(HttpRequest, Int), (Try[HttpResponse], Int), Http.HostConnectionPool] =
    Http()
      .cachedHostConnectionPool[Int]("www.google.com")

  Source(1 to 10)
    .map(i => (HttpRequest(), i))
    .via(poolFlow)
    .map {
      case (Success(response), value) =>
        // VERY IMPORTANT
        response.discardEntityBytes()
        // without this it will block the connection that this response wants to get through
        // it leads to leaking connections from the pool
        s"Request $value has received response: $response"
      case (Failure(ex), value) =>
        s"Request $value has failed: $ex"
    }
  //.runWith(Sink.foreach[String](println))

  import PaymentSystemDomain._

  private val creditCards = List(
    CreditCard("4242-4242-4242-4242", "424", "tx-test-account"),
    CreditCard("1234-1234-1234-1234", "123", "tx-daniel-account"),
    CreditCard("1234-1234-4321-4321", "321", "my-awesome-account"),
  )

  private val paymentRequests = creditCards.map(creditCard =>
    PaymentRequest(creditCard, "rtjvm-store-account", 99))
  private val serverHttpRequests: Seq[(HttpRequest, String)] = paymentRequests.map(paymentRequest =>
    (
      HttpRequest(
        HttpMethods.POST,
        uri = Uri("/api/payments"),
        entity = HttpEntity(
          ContentTypes.`application/json`,
          paymentRequest.toJson.prettyPrint
        )
      ),
      UUID.randomUUID().toString
    )
  )

  Source(serverHttpRequests)
    .via(Http().cachedHostConnectionPool[String]("localhost", 8080))
    .runForeach { // (Try[HttpResponse], String)
      case (Success(response@HttpResponse(StatusCodes.Forbidden, _, _, _)), orderId) =>
        println(s"The order ID $orderId was not allowed to proceed: $response")
      case (Success(response), orderId) =>
        println(s"The order Id $orderId was successful and returned the response: $response")
      // do something with the order ID: dispatch it, send a notification to the customer, etc
      case (Failure(ex), orderId) =>
        println(s"The order ID $orderId could not be completed: $ex")
    }
}
