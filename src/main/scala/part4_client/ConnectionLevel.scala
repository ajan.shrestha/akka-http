package part4_client

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, HttpResponse, Uri}
import akka.stream.scaladsl.{Sink, Source}
import spray.json._

import scala.concurrent.Future
import scala.util.{Failure, Success}

object ConnectionLevel extends App with PaymentJsonProtocol {
  /*
   * Goal
   *  Multiple client API levels
   *    - connection-level API (lowest level with most control)
   *    - host-level API (mid)
   *    - request-level API (easiest to use)
   *
   * This: Connection-level API
   *  - A connection works as a Flow[HttpRequest, HttpResponse, _]
   *    - send off-requests(not recommended)
   *      - coz it materializes the Akka stream every single time and starts
   *        TCO connections every single time.
   *    - work with streams of requests and responses
   *  - HTTPS: enable it in a connection context
   */
  implicit private val system: ActorSystem = ActorSystem("ConnectionLevel")

  import system.dispatcher

  private val connectionFlow = Http().outgoingConnection("www.google.com")

  private def oneOffRequest(request: HttpRequest): Future[HttpResponse] =
    Source.single(request).via(connectionFlow).runWith(Sink.head)

  oneOffRequest(HttpRequest()).onComplete {
    case Success(response) => println(s"Got successful response: $response")
    case Failure(ex) => println(s"Sending the request failed: $ex")
  }

  /*
   * A small payments system
   */

  import PaymentSystemDomain._

  private val creditCards = List(
    CreditCard("4242-4242-4242-4242", "424", "tx-test-account"),
    CreditCard("1234-1234-1234-1234", "123", "tx-daniel-account"),
    CreditCard("1234-1234-4321-4321", "321", "my-awesome-account"),
  )

  private val paymentRequests = creditCards.map(creditCard =>
    PaymentRequest(creditCard, "rtjvm-store-account", 99))
  private val serverHttpRequests = paymentRequests.map(paymentRequest =>
    HttpRequest(
      HttpMethods.POST,
      uri = Uri("/api/payments"),
      entity = HttpEntity(
        ContentTypes.`application/json`,
        paymentRequest.toJson.prettyPrint
      )
    )
  )

  Source(serverHttpRequests)
    .via(Http().outgoingConnection("localhost", 8080))
    .to(Sink.foreach[HttpResponse](println))
    .run()
}
