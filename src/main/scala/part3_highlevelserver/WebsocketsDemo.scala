package part3_highlevelserver

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.model.ws.{BinaryMessage, Message, TextMessage}
import akka.http.scaladsl.server.Directives._
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.util.CompactByteString

import scala.concurrent.duration.DurationInt

object WebsocketsDemo extends App {
  /*
   * Goal
   *  - Modern protocol for bi-directional communication between client/frontend and server
   *  - A websocket is established by an upgraded HTTP/HTTPS connections, and
   *    this is established by the exchange of special headers
   *      - Connection: Upgrade
   *      - Upgrade: websocket
   *  - Once the server understands these headers, it will upgrade the connection to a
   *    websocket, which will then establish a medium for exchanging messages.
   *  - These messages can be sent in both ways, and in particular, the server can also
   *    push some data to client.
   */
  implicit private val system: ActorSystem = ActorSystem("WebsocketsDemo")

  import system.dispatcher

  // Message: TextMessage vs BinaryMessage
  // BinaryMessage -> turn anything into bytes; sounds, videos, images, etc

  // Text Message is a wrapper over sources.
  // Reason: the messages that can be exchanged in both ways can have an
  //  arbitrary size, which is why Akka Http models those as streams.
  private val textMessage = TextMessage(Source.single("hello via a text message"))
  private val binaryMessage = BinaryMessage(Source.single(CompactByteString("hello via a binary message")))

  private val html =
    """
      |<html lang="">
      |<head>
      |    <script>
      |
      |        const exampleSocket = new WebSocket("ws://localhost:8080/greeter");
      |        console.log("starting websocket...");
      |
      |        exampleSocket.onmessage = function (event) {
      |            const newChild = document.createElement("div");
      |            newChild.innerText = event.data;
      |            document.getElementById("1").appendChild(newChild);
      |        }
      |
      |        exampleSocket.onopen = function (event) {
      |            exampleSocket.send("socket seems to be open...");
      |        }
      |
      |        exampleSocket.send("socket says: hello, server!");
      |    </script>
      |    <title>WebsocketsDemo</title>
      |</head>
      |<body>
      |Starting websocket...
      |<div id="1"></div>
      |</body>
      |</html>
      |""".stripMargin

  private val websocketFlow: Flow[Message, Message, Any] = Flow[Message].map {
    case tm: TextMessage =>
      TextMessage(Source.single("Server says back:") ++ tm.textStream ++ Source.single("!"))
    case bm: BinaryMessage =>
      // need to drain the content of the binary message in order to not clog the websocket
      // otherwise we could leak resources
      bm.dataStream.runWith(Sink.ignore)
      TextMessage(Source.single("Server received a binary message..."))
  }

  private val websocketRoute =
    (pathEndOrSingleSlash & get) {
      complete(HttpEntity(
        ContentTypes.`text/html(UTF-8)`,
        html
      ))
    } ~
      path("greeter") {
        handleWebSocketMessages(socialFlow)
      }

  Http()
    .newServerAt("localhost", 8080)
    .bind(websocketRoute)

  private case class SocialPost(owner: String, content: String)

  private val socialFeed = Source(
    List(
      SocialPost("Martin", "Scala 3 has been announced!"),
      SocialPost("Daniel", "A new Rock the JVM course is open!"),
      SocialPost("Martin", "I killed Java.")
    )
  )

  private val socialMessages = socialFeed
    .throttle(1, 2.seconds)
    .map(socialPost => TextMessage(s"${socialPost.owner} said: ${socialPost.content}"))

  private val socialFlow: Flow[Message, Message, Any] = Flow.fromSinkAndSource(
    Sink.foreach[Message](println),
    socialMessages
  )
}
