package part3_highlevelserver

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.util.Timeout
import part2_lowlevelserver.{Guitar, GuitarDB, GuitarStoreJsonProtocol}
import spray.json._

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt


object HighLevelExample extends App with GuitarStoreJsonProtocol {
  /*
   * Goal
   *  A bigger example with multiple routes
   *  An exercise to practice directives and routes
   */
  implicit private val system: ActorSystem = ActorSystem("HighLevelExample")

  import system.dispatcher
  import GuitarDB._

  /*
   * GET  /api/guitar                               fetches ALL the guitars in the store
   * GET  /api/guitar?id=X                          fetches the guitar with id X
   * GET  /api/guitar/X                             fetches the guitar with id X
   * GET  /api/guitar/inventory?inStock=true/false
   */

  /*
   * setup
   */
  private val guitarDb = system.actorOf(Props[GuitarDB], "LowLevelGuitarDB")
  private val guitarList = List(
    Guitar("Fender", "Stratocaster"),
    Guitar("Gibson", "Les Paul"),
    Guitar("Martin", "LX1"),
  )

  guitarList.foreach { guitar =>
    guitarDb ! CreateGuitar(guitar)
  }

  implicit private val timeout: Timeout = Timeout(2.seconds)
  /*private val guitarServerRoute =
    path("api" / "guitar") {
      // ALWAYS PUT THE MORE SPECIFIC ROUTE FIRST
      parameter(Symbol("id").as[Int]) { (guitarId: Int) =>
        get {
          val guitarFuture: Future[Option[Guitar]] = (guitarDb ? FindGuitar(guitarId)).mapTo[Option[Guitar]]
          val entityFuture = guitarFuture.map { guitarOption =>
            HttpEntity(
              ContentTypes.`application/json`,
              guitarOption.toJson.prettyPrint
            )
          }
          complete(entityFuture)
        }
      } ~
        get {
          val guitarsFuture: Future[List[Guitar]] = (guitarDb ? FindAllGuitars).mapTo[List[Guitar]]
          val entityFuture = guitarsFuture.map { guitars =>
            HttpEntity(
              ContentTypes.`application/json`,
              guitars.toJson.prettyPrint
            )
          }

          complete(entityFuture)
        }
    } ~
      path("api" / "guitar" / IntNumber) { (guitarId: Int) =>
        get {
          val guitarFuture: Future[Option[Guitar]] = (guitarDb ? FindGuitar(guitarId)).mapTo[Option[Guitar]]
          val entityFuture = guitarFuture.map { guitarOption =>
            HttpEntity(
              ContentTypes.`application/json`,
              guitarOption.toJson.prettyPrint
            )
          }
          complete(entityFuture)
        }
      } ~
      path("api" / "guitar" / "inventory") {
        get {
          parameter(Symbol("inStock").as[Boolean]) { inStock =>
            val guitarFuture: Future[List[Guitar]] = (guitarDb ? FindGuitarsInStock(inStock)).mapTo[List[Guitar]]
            val entityFuture = guitarFuture.map { guitars =>
              HttpEntity(
                ContentTypes.`application/json`,
                guitars.toJson.prettyPrint
              )
            }
            complete(entityFuture)
          }
        }
      }*/

  private def toHttpEntity(payload: String): HttpEntity.Strict =
    HttpEntity(ContentTypes.`application/json`, payload)

  private val simplifiedGuitarServerRoute =
    (pathPrefix("api" / "guitar") & get) {
      path("inventory") {
        parameter(Symbol("inStock").as[Boolean]) { inStock =>
          complete(
            (guitarDb ? FindGuitarsInStock(inStock))
              .mapTo[List[Guitar]]
              .map(_.toJson.prettyPrint)
              .map(toHttpEntity))
        }
      } ~
        (path(IntNumber) | parameter(Symbol("id").as[Int])) { (guitarId: Int) =>
          complete((guitarDb ? FindGuitar(guitarId))
            .mapTo[Option[Guitar]]
            .map(_.toJson.prettyPrint)
            .map(toHttpEntity))
        } ~
        pathEndOrSingleSlash {
          complete((guitarDb ? FindAllGuitars)
            .mapTo[List[Guitar]]
            .map(_.toJson.prettyPrint)
            .map(toHttpEntity))
        }
    }

  Http()
    .newServerAt("localhost", 8080)
    .bind(simplifiedGuitarServerRoute)
}
