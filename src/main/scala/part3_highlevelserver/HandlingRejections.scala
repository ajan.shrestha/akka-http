package part3_highlevelserver

import akka.actor.ActorSystem
import akka.http.javadsl.server.{MethodRejection, MissingQueryParamRejection}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Rejection, RejectionHandler, Route}

object HandlingRejections extends App {
  /*
   * Rejections
   *  If a request doesn't match a filter directive, it's rejected
   *    - reject = pass the request to another branch in the routing tree
   *    - a rejection is NOT a failure
   *
   *  Rejections are aggregated
   *    - if request not consumed, add rejection
   *    - ...
   *    - if request consumed, clear the rejection list
   *      (other rejections might be added within)
   *  We can choose how to handle the rejection list
   *
   * Handle rejections
   *  - rejection handler = function between a rejection list and a route
   *  - rejection handling directives
   * Implicit rejection handlers and builders
   */
  implicit private val system: ActorSystem = ActorSystem("HandlingRejections")

  import system.dispatcher

  // Route.seal to take in implicit rejections
  private val simpleRoute = Route.seal(
    path("api" / "myEndpoint") {
      get {
        complete(StatusCodes.OK)
      } ~
        parameter(Symbol("id")) { _ =>
          complete(StatusCodes.OK)
        }
    })

  // Rejection handlers
  private val badRequestHandler: RejectionHandler = { rejections: Seq[Rejection] =>
    println(s"I have encountered rejections: $rejections")
    Some(complete(StatusCodes.BadRequest))
  }

  private val forbiddenHandler: RejectionHandler = { rejections: Seq[Rejection] =>
    println(s"I have encountered rejections: $rejections")
    Some(complete(StatusCodes.Forbidden))
  }

  private val simpleRouteWithHandlers =
    handleRejections(badRequestHandler) { // handle rejections from the top level
      // define server logic inside
      path("api" / "myEndpoint") {
        get {
          complete(StatusCodes.OK)
        } ~
          post {
            handleRejections(forbiddenHandler) { // handle rejections WITHIN
              parameter(Symbol("myParam")) { myParam =>
                complete(StatusCodes.OK)
              }
            }
          }
      }
    }

  // RejectionHandler.default - top-level rejection

  implicit def customRejectionHandler: RejectionHandler = RejectionHandler
    .newBuilder()
    .handle {
      case m: MissingQueryParamRejection =>
        println(s"I got a query param rejection: $m")
        complete("Rejected query param!")
    }
    .handle {
      case m: MethodRejection =>
        println(s"I got a method rejection: $m")
        complete("Rejected method")
    }
    .result()

  // sealing a route - implicit RejectionHandler

  Http()
    .newServerAt("localhost", 8080)
    // .bind(simpleRouteWithHandlers)
    .bind(simpleRoute)
}
