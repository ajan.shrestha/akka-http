package part3_highlevelserver

import akka.actor.ActorSystem
import akka.event.LoggingAdapter
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpRequest, StatusCodes}
import akka.http.scaladsl.server.Directives._

object DirectivesBreakdown extends App {
  /*
   * Goal
   *  Familiarize with all the most popular directives
   *  Understand filtering, chaining, extraction and transformation
   *
   * What a Route Is
   *  type Route = RequestContext => Future[RouteResult]
   *    - A RequestContext contains
   *      - the HttpRequest being handled
   *      - the actor system
   *      - the actor materializer
   *      - the logging adapter
   *      - routing settings
   *      - etc
   *    - This is the data structure handled by a Route
   *    - Directives create Routes; composing routes creates a routing tree
   *      - filtering and nesting
   *      - chaining with ~
   *      - extracting data
   *
   * What a Route can do with a RequestContext:
   *  - complete it synchronously with a response
   *  - complete it asynchronously with a Future(response)
   *  - handle it asynchronously by returning a Source(advanced)
   *    - streams data back to the client
   *  - reject it and pass it on to the next Route
   *    - rejecting a RequestContext, meaning that a route does not accept
   *      that HTTP request but the next route might accept it
   *  - fail it
   */
  implicit private val system: ActorSystem = ActorSystem("DirectivesBreakdown")

  import system.dispatcher

  /*
   * Type #1: filtering directives
   */
  private val simpleHttpMethodRoute =
    post { // equivalent directives for get, put, patch, delete, head, options
      complete(StatusCodes.Forbidden)
    }

  private val simplePathRoute =
    path("about") {
      complete(
        HttpEntity(
          ContentTypes.`text/html(UTF-8)`,
          """
            |<html>
            | <body>
            |   Hello from the about page!
            | </body>
            |</html>
            |""".stripMargin
        )
      )
    }

  private val complexRoute =
    path("api" / "myEndpoint") {
      complete(StatusCodes.OK)
    } // /api/myEndpoint

  private val dontConfuse =
    path("api/myEndpoint") { // Url encoded
      complete(StatusCodes.OK)
    }

  private val pathEndRoute =
    pathEndOrSingleSlash { // localhost:8080 or localhost:8080/
      complete(StatusCodes.OK)
    }

  /*
   * Type #2: extraction directives
   */

  // GET  on /api/item/42
  private val pathExtractionRoute =
    path("api" / "item" / IntNumber) { (itemNumber: Int) =>
      // other directives
      println(s"I've got a number in my path: $itemNumber")
      complete(StatusCodes.OK)
    }

  private val pathMultiExtractRoute =
    path("api" / "order" / IntNumber / IntNumber) { (id, inventory) =>
      println(s"I've got TWO numbers in my path: $id, $inventory")
      complete(StatusCodes.OK)
    }

  private val queryParamExtractionRoute =
  // /api/item?id=45
    path("api" / "item") {
      /*
       * Good practice:
       *  - with the parameter directive
       *    - pass the parameter name as symbols instead of strings
       *    - Symbol(<name>)
       *    - Symbols are automatically interned into the JVM,
       *      meaning that they are held in a special memory zone
       *      and they offer performance benefits because they're always
       *      compared by reference equality instead of content equality
       */
      parameter(Symbol("id").as[Int]) { (itemId: Int) =>
        println(s"I've extracted the ID as $itemId")
        complete(StatusCodes.OK)
      }
    }

  private val extractRequestRoute =
    path("controlEndpoint") {
      extractRequest { (httpRequest: HttpRequest) =>
        extractLog { (log: LoggingAdapter) =>
          log.info(s"I've got the http request: $httpRequest")
          complete(StatusCodes.OK)
        }
      }
    }

  /*
   * Type #3: composite directives
   */
  private val simpleNestedRoute =
    path("api" / "item") {
      get {
        complete(StatusCodes.OK)
      }
    }

  private val compactSimpleNestedRoute =
    (path("api" / "item") & get) {
      complete(StatusCodes.OK)
    }

  private val compactExtractRequestRoute =
    (path("controlEndpoint") & extractRequest & extractLog) { (request, log) =>
      log.info(s"I've got the http request: $request")
      complete(StatusCodes.OK)
    }

  // /about and /aboutUs
  private val repeatedRoute =
    path("about") {
      complete(StatusCodes.OK)
    } ~
      path("aboutUs") {
        complete(StatusCodes.OK)
      }

  private val dryRoute =
    (path("about") | path("aboutUs")) {
      complete(StatusCodes.OK)
    }

  // yourblog.com/42 AND yourblog.com?postId=42
  private val blogByIdRoute =
    path(IntNumber) { (blogpostId: Int) =>
      // complex server logic
      complete(StatusCodes.OK)
    }

  private val blogByQueryParamRoute =
    parameter(Symbol("postId").as[Int]) { (blogpostId: Int) =>
      // the SAME server logic
      complete(StatusCodes.OK)
    }

  private val combinedBlogByIdRoute =
    (path(IntNumber) | parameter(Symbol("postId").as[Int])) { (blogpostId: Int) =>
      // your original server logic
      complete(StatusCodes.OK)
    }

  /*
   * Type #4: "actionable" directives
   */
  private val completeOkRoute = complete(StatusCodes.OK)

  private val failRoute =
    path("notSupported") {
      failWith(new RuntimeException("Unsupported!")) // completes with HTTP 500
    }

  private val routeWithRejection =
  /*path("home") {
    reject
  } ~*/
    path("index") {
      complexRoute
    }

  /*
   * Exercise
   */
  private val getOrPutPath =
    path("api" / "myEndpoint") {
      get {
        completeOkRoute
      } ~
      post {
        complete(StatusCodes.Forbidden)
      }
    }

  Http()
    .newServerAt("localhost", 8080)
    .bind(getOrPutPath)
}
