package part3_highlevelserver

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import part2_lowlevelserver.HttpsContext

object HighLevelIntro extends App {
  /*
   * Goal
   *  Familiarize with the high-level server API (routing DSL)
   *    - Routes
   *    - Directives
   */

  implicit private val system: ActorSystem = ActorSystem("HighLevelIntro")

  import system.dispatcher

  /*
   * Directives
   *  - building blocks of high level HTTP server logic
   *  - specify what happens, under which condition
   *  - can compose directives inside on another
   *  - chain with ~
   *
   * Route
   *  -
   */

  private val simpleRoute: Route =
    path("home") { // DIRECTIVE - here filter out HttpRequest
      complete(StatusCodes.OK) // DIRECTIVE - here send out an HttpResponse
    }

  private val pathGetRoute: Route =
    path("home") {
      get {
        complete(StatusCodes.OK)
      }
    }

  // chaining directives with ~
  private val chainedRoute: Route = {
    path("myEndpoint") {
      get {
        complete(StatusCodes.OK)
      } /* VERY IMPORTANT ---> */ ~
        post {
          complete(StatusCodes.Forbidden)
        }
    } ~
      path("home") {
        complete(
          HttpEntity(
            ContentTypes.`text/html(UTF-8)`,
            """
              |<html>
              | <body>
              |   Hello from the high level Akka HTTP!
              | </body>
              |</html>
              |""".stripMargin
          )
        )
      }
  } // Routing tree

  Http()
    .newServerAt("localhost", 8080)
    .enableHttps(HttpsContext.httpsConnectionContext)
    .bind(pathGetRoute)
}
