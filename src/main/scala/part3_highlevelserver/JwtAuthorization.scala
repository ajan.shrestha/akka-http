package part3_highlevelserver

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Directives._
import pdi.jwt.{JwtAlgorithm, JwtClaim, JwtSprayJson}
import spray.json._

import java.util.concurrent.TimeUnit
import scala.util.{Failure, Success}

object SecurityDomain extends DefaultJsonProtocol {
  case class LoginRequest(username: String, password: String)

  implicit val loginRequestFormat: RootJsonFormat[LoginRequest] = jsonFormat2(LoginRequest)
}

object JwtAuthorization extends App with SprayJsonSupport {
  /*
   * Goal
   *  JWT -> JSON web Tokens
   *    - encoded strings which represent some JSON data
   *    - used for
   *      - authorization
   *      - exchange of data
   *
   *  Not Akka HTTP specific, but often used in web apps/microservices
   *
   * JWT Authorization
   *  Principles
   *    - you authenticate to the server (username + pass, OAuth,etc)
   *    - server sends you back a string aka token
   *    - you then use that string for secure endpoints
   *      - special HTTP header Authorization: (token)
   *      - the endpoint will check the token for permissions
   *      - your call is allowed/rejected
   *
   *  Result: authorization
   *    - not authentication: you receive the token after authentication
   *
   * JWT Structure
   *  Part 1: header
   *    {
   *      "typ": "JWT",
   *      "alg": "HS256
   *    }
   *    => Base64 encode
   *
   *  Part 2: payload (claims)
   *    {
   *      "iss": "rockthejvm.com",
   *      "exp": 1300819380,
   *      "name": "Daniel Ciocirlan",
   *      "admin": true
   *    }
   *    => Base64 encode
   *    Registered claims(standard)
   *      - issuer
   *      - expiration date
   *    Public claims (custom)
   *
   *  Part 3: signature
   *    - take encoded header + "." + encoded claims
   *    - sign with the algorithm in the header and a secret key
   *    - encode base64
   */
  implicit private val system: ActorSystem = ActorSystem("JwtAuthorization")

  import system.dispatcher
  import SecurityDomain._

  private val superSecretPasswordDb = Map(
    "admin" -> "admin",
    "daniel" -> "Rockthejvm1!"
  )

  private def checkPassword(username: String, password: String): Boolean =
    superSecretPasswordDb.contains(username) && superSecretPasswordDb(username) == password

  private val algorithm = JwtAlgorithm.HS256
  private val secretKey = "rockthjvmsecret"

  private def createToken(username: String, expirationPeriodInDays: Int): String = {
    val claims = JwtClaim(
      expiration = Some(System.currentTimeMillis() / 1000 + TimeUnit.DAYS.toSeconds(expirationPeriodInDays)),
      issuedAt = Some(System.currentTimeMillis() / 1000),
      issuer = Some("rockthejvm.com"),
      content = ""
    )

    JwtSprayJson.encode(claims, secretKey, algorithm) //  JWT string
  }

  private def isTokenExpired(token: String): Boolean = JwtSprayJson.decode(token, secretKey, Seq(algorithm)) match {
    case Success(claims) => claims.expiration.getOrElse(0L) < System.currentTimeMillis() / 1000
    case Failure(_) => true
  }

  private def isTokenValid(token: String): Boolean = JwtSprayJson.isValid(token, secretKey, Seq(algorithm))

  private val loginRoute =
    post {
      entity(as[LoginRequest]) {
        case LoginRequest(username, password) if checkPassword(username, password) =>
          val token = createToken(username, 1)
          respondWithHeader(RawHeader("Access-Token", token)) {
            complete(StatusCodes.OK)
          }
        case _ => complete(StatusCodes.Unauthorized)
      }
    }

  private val authenticatedRoute =
    (path("secureEndpoint") & get) {
      optionalHeaderValueByName("Authorization") {
        case Some(token) =>
          if (isTokenValid(token)) {
            if (isTokenExpired(token)) {
              complete(HttpResponse(status = StatusCodes.Unauthorized, entity = "Token expired."))
            } else {
              complete("User accessed authorized endpoint!")
            }
          } else {
            complete(
              HttpResponse(
                status = StatusCodes.Unauthorized,
                entity = "Token is invalid, or has been tampered with."))
          }
        case _ =>
          complete(
            HttpResponse(
              status = StatusCodes.Unauthorized,
              entity = "No token provided!"))
      }
    }

  private val route = loginRoute ~ authenticatedRoute

  Http()
    .newServerAt("localhost", 8080)
    .bind(route)
}
