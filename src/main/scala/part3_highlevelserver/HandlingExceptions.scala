package part3_highlevelserver

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ExceptionHandler, Route}

object HandlingExceptions extends App {
  /*
   * Exceptions
   *  Serious business: exceptions are not rejections
   *  Bright side: easier to handle
   *    - exceptions are not aggregated by routing tree
   *    - (exceptions) handle by the first exception handler as they bubble up
   *    - exception handlers don't have the problem of handling exceptions by priority
   */

  implicit private val system: ActorSystem = ActorSystem("HandlingExceptions")

  import system.dispatcher

  // Route.seal to add implicit exceptions
  private val simpleRoute = Route.seal(
    path("api" / "people") {
      get {
        // directive that throws some exception
        throw new RuntimeException("Getting all the people took too long")
      } ~
        post {
          parameter(Symbol("id")) { id =>
            if (id.length > 2)
              throw new NoSuchElementException(s"Parameter $id cannot be found in the database, TABLE FLIP!")
            complete(StatusCodes.OK)
          }
        }
    })

  implicit private val customExceptionHandler: ExceptionHandler = ExceptionHandler {
    case e: RuntimeException =>
      complete(StatusCodes.NotFound, e.getMessage)
    case e: IllegalArgumentException =>
      complete(StatusCodes.BadRequest, e.getMessage)
  }

  private val runtimeExceptionHandler: ExceptionHandler = ExceptionHandler {
    case e: RuntimeException =>
      complete(StatusCodes.NotFound, e.getMessage)
  }

  private val noSuchElementExceptionHandler: ExceptionHandler = ExceptionHandler {
    case e: NoSuchElementException =>
      complete(StatusCodes.BadRequest, e.getMessage)
  }

  private val delicateHandleRoute =
    handleExceptions(runtimeExceptionHandler) {
      path("api" / "people") {
        get {
          // directive that throws some exception
          throw new RuntimeException("Getting all the people took too long")
        } ~
          handleExceptions(noSuchElementExceptionHandler) {
            post {
              parameter(Symbol("id")) { id =>
                if (id.length > 2)
                  throw new NoSuchElementException(s"Parameter $id cannot be found in the database, TABLE FLIP!")
                complete(StatusCodes.OK)
              }
            }
          }
      }
    }


  Http()
    .newServerAt("localhost", 8080)
    // .bind(simpleRoute)
    .bind(delicateHandleRoute)
}
