package part1_recap

import akka.actor.ActorSystem
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}

import scala.util.{Failure, Success}

object AkkaStreamsRecap extends App {

  /*
   * Goal
   *  Akka HTTP is based on Akka Streams
   *    - Basic blocks: Source, Sink and Flow
   *    - Materialization
   *    - Backpressure
   *    - Async boundaries
   */

  implicit private val system: ActorSystem = ActorSystem("AkkaStreamsRecap")
  // Actor Materializer is implicit from ActorSystem
  // it allocates the right resources for constructing Akka stream components.

  import system.dispatcher

  private val source = Source(1 to 100)
  private val sink = Sink.foreach[Int](println)
  private val flow = Flow[Int].map(_ + 1)

  private val runnableGraph = source.via(flow).to(sink)
  // private val simpleMaterializedValue = runnableGraph.run() // materialization

  // MATERIALIZED VALUE => extract meaningful value from the graph
  private val sumSink = Sink.fold[Int, Int](0)((currentSum, element) => currentSum + element)
  /*private val sumFuture = source.runWith(sumSink)

  sumFuture.onComplete {
    case Success(sum) => println(s"The sum of all the numbers from the simple source is: $sum")
    case Failure(exception) => println(s"Summing all the numbers from the simple source FAILED: $exception")
  }*/

  private val anotherMaterializedValue = source
    .viaMat(flow)(Keep.right)
    .toMat(sink)(Keep.left)
    //.run()

  /*
   * - materializing a graph means materializing ALL the components // can choose the value
   * - a materialized value can be ANYTHING AT ALL
   */

  /*
   * Backpressure actions
   *
   *  - buffer elements
   *  - apply a strategy in case the buffer overflows
   *  - fail the entire stream
   */

  private val bufferedFlow = Flow[Int].buffer(10, OverflowStrategy.dropHead)

  source.async
    .via(bufferedFlow).async
    .runForeach { e =>
      // a slow consumer
      Thread.sleep(100)
      println(e)
    }
}
