package part1_recap


import scala.concurrent.Future
import scala.language.implicitConversions
import scala.util.{Failure, Success}


object ScalaRecap extends App {

  private val aCondition: Boolean = false

  private def myFunction(x: Int) = {
    // code
    if (x > 4) 42 else 65
  }

  // instructions(executed) vs expressions(evaluated)
  // types + type inference

  // OO features of Scala
  class Animal

  trait Carnivore {
    def eat(a: Animal): Unit
  }

  object Carnivore

  // generics
  abstract class MyList[+A]

  // method notations
  1 + 2 // infix notation
  1.+(2)

  // FP
  // val anIncrementer: Function1[Int, Int]
  private val anIncrementer: Int => Int = (x: Int) => x + 1
  anIncrementer(1)

  List(1, 2, 3).map(anIncrementer)
  // HOF: flatMap, filter
  // for-comprehension

  // Monads: Option, Try

  // Pattern matching:
  private val unknown: Any = 2
  private val order = unknown match {
    case 1 => "first"
    case 2 => "second"
    case _ => "unknown"
  }

  try {
    // code that can thrown an exception
    throw new RuntimeException
  } catch {
    case e: Exception => println("I caught one!")
  }

  /*
   * Scala advanced
   */
  // multithreading

  import scala.concurrent.ExecutionContext.Implicits.global

  private val future = Future {
    // long computation here
    // executed on SOME other thread
    42
  }

  // map, flatMap, filter + other niceties e.g. recover.recoverWith

  future.onComplete {
    case Success(value) => println(s"I found the meaning of life: $value")
    case Failure(exception) => println(s"I found $exception while searching for the meaning of life!")
  } // on SOME thread

  private val partialFunction: PartialFunction[Int, Int] = {
    case 1 => 42
    case 2 => 65
    case _ => 999
  } // based on pattern matching!

  // type aliases
  private type AkkaReceive = PartialFunction[Any, Unit]

  private def receive: AkkaReceive = {
    case 1 => println("hello!")
    case _ => println("confused...")
  }

  // Implicits!
  implicit val timeout: Int = 3000

  private def setTimeout(f: () => Unit)(implicit timeout: Int): Unit = f()

  setTimeout(() => println("timeout")) // other arg list injected by the compiler

  // conversions
  // 1. implicit methods
  case class Person(name: String) {
    def greet: String = s"Hi, my name is $name"
  }

  implicit def fromStringToPerson(name: String): Person = Person(name)

  "Peter".greet
  // fromStringToPerson("Peter").greet

  // 2. implicit classes
  implicit class Dog(name: String) {
    def bark: Unit = println("Bark!")
  }

  "Lassie".bark
  // new Dog("Lassie").bark

  // implicit organizations
  // local scope
  implicit val numberOrdering: Ordering[Int] = Ordering.fromLessThan(_ > _)
  List(1, 2, 3).sorted //(numberOrdering) => List(3,2,1)

  // imported scope

  // companion objects of the types involved in the call
  private object Person {
    implicit val personOrdering: Ordering[Person] = Ordering.fromLessThan((a, b) => a.name.compareTo(b.name) < 0)
  }

  List(Person("Bob"), Person("Alice")).sorted // (Person.personOrdering)
  // => List(Person("Alice"), Person("Bob"))
}
