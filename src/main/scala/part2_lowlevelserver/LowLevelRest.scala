package part2_lowlevelserver

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, HttpResponse, StatusCodes, Uri}
import akka.pattern.ask
import akka.util.Timeout
import spray.json.RootJsonFormat

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
// step 1
import spray.json.{DefaultJsonProtocol, enrichAny, enrichString}

case class Guitar(make: String, model: String, quantity: Int = 0)

object GuitarDB {
  case class CreateGuitar(guitar: Guitar)

  case class GuitarCreated(id: Int)

  case class FindGuitar(id: Int)

  case object FindAllGuitars

  case class AddQuantity(id: Int, quantity: Int)

  case class FindGuitarsInStock(inStock: Boolean)
}

class GuitarDB extends Actor with ActorLogging {

  import GuitarDB._

  private var guitars: Map[Int, Guitar] = Map()
  private var currentGuitarId: Int = 0

  override def receive: Receive = {
    case FindAllGuitars =>
      log.info("Searching for all guitars")
      sender() ! guitars.values.toList

    case FindGuitar(id) =>
      log.info(s"Searching guitar by id: $id")
      sender() ! guitars.get(id)

    case CreateGuitar(guitar) =>
      log.info(s"Adding guitar $guitar with id $currentGuitarId")
      guitars = guitars + (currentGuitarId -> guitar)
      sender() ! GuitarCreated(currentGuitarId)
      currentGuitarId += 1

    case AddQuantity(id, quantity) =>
      log.info(s"Trying to add $quantity items for guitar $id")
      val guitar: Option[Guitar] = guitars.get(id)
      val newGuitar: Option[Guitar] = guitar.map {
        case Guitar(make, model, q) => Guitar(make, model, q + quantity)
      }

      newGuitar.foreach(guitar => guitars = guitars + (id -> guitar))
      sender() ! newGuitar

    case FindGuitarsInStock(inStock) =>
      log.info(s"Searching for all guitars ${if (inStock) "in" else "out of"} stock")
      if (inStock)
        sender() ! guitars.values.toList.filter(_.quantity > 0)
      else
        sender() ! guitars.values.toList.filter(_.quantity == 0)
  }
}

// step 2
trait GuitarStoreJsonProtocol extends DefaultJsonProtocol {

  // step 3
  // jsonFormatX
  // X => case class has X parameters
  implicit val guitarFormat: RootJsonFormat[Guitar] = jsonFormat3(Guitar)
}

// step 4 - bring the format into scope
object LowLevelRest extends App with GuitarStoreJsonProtocol {
  /*
   * Goal
   *  Handle JSON payloads in requests/responses with spray-json
   *    - REST APIs are built on top of HTTP to transfer information by using
   *      HTTP status codes to track successes and failures.
   *    - The payloads for these HTTP requests and responses are usually serialized
   *      in JSON format - the JavaScript object notation
   */
  implicit private val system: ActorSystem = ActorSystem("LowLevelRest")

  import system.dispatcher // just for example here in Prod create dedicated dispatchers
  // running futures may starve the ActorSystem for thread
  import GuitarDB._

  implicit private val defaultTimeout: Timeout = Timeout(2.seconds)

  /*
   * - GET  /api/guitar       => ALL the guitars in the store
   * - GET  /api/guitar?id=X  => fetches the guitar associated with id X
   * - POST /api/guitar       => insert the guitar into the store
   */

  // JSON -> marshalling
  // the process of serializing our data to a wire format that an HTTP client
  // can understand
  private val simpleGuitar = Guitar("Fender", "Stratocaster")
  // toJson -> converts to intermediate JSON AST
  // prettyPrint -> JSON object to string
  println(simpleGuitar.toJson.prettyPrint)

  // unmarshalling
  private val simpleGuitarJsonString =
    """
      |{
      |  "make": "Fender",
      |  "model": "Stratocaster",
      |  "quantity": 1
      |}
      |""".stripMargin
  println(simpleGuitarJsonString.parseJson.convertTo[Guitar])

  /*
   * setup
   */
  private val guitarDb = system.actorOf(Props[GuitarDB], "LowLevelGuitarDB")
  private val guitarList = List(
    Guitar("Fender", "Stratocaster"),
    Guitar("Gibson", "Les Paul"),
    Guitar("Martin", "LX1"),
  )

  guitarList.foreach { guitar =>
    guitarDb ! CreateGuitar(guitar)
  }

  /*
   * server code
   */
  private def getGuitar(query: Uri.Query): Future[HttpResponse] = {
    val guitarId = query.get("id").map(_.toInt) // Option[Int] // Handle Error handling in prod

    guitarId match {
      case None => Future(HttpResponse(StatusCodes.NotFound)) // /api/guitar?id=
      case Some(id: Int) =>
        val guitarFuture: Future[Option[Guitar]] = (guitarDb ? FindGuitar(id)).mapTo[Option[Guitar]]
        guitarFuture.map {
          case None => HttpResponse(StatusCodes.NotFound) // /api/guitar?id=9999 ;9999 doesn't exist
          case Some(guitar) => HttpResponse(
            entity = HttpEntity(
              ContentTypes.`application/json`,
              guitar.toJson.prettyPrint
            )
          )
        }
    }
  }

  private val requestHandler: HttpRequest => Future[HttpResponse] = {
    case HttpRequest(HttpMethods.GET, uri@Uri.Path("/api/guitar"), _, _, _) =>
      /*
       * query parameters handling code
       */
      val query = uri.query() // query object => map[String, String]
      if (query.isEmpty) {
        val guitarsFuture: Future[List[Guitar]] = (guitarDb ? FindAllGuitars).mapTo[List[Guitar]]
        guitarsFuture.map { guitars =>
          HttpResponse(
            entity = HttpEntity(
              ContentTypes.`application/json`,
              guitars.toJson.prettyPrint
            )
          )
        }
      } else {
        // fetch guitar associated to the guitar id
        // /api/guitar?id=42
        getGuitar(query)
      }

    case HttpRequest(HttpMethods.POST, Uri.Path("/api/guitar"), _, entity, _) =>
      // entities are a Source[ByteString]
      // the entities may be large therefore need to convert to StrictEntity
      // Akka Http will attempt to bring all the contents of this entity
      // into memory from the HTTP connection during the course of 3 seconds
      val strictEntityFuture = entity.toStrict(3.seconds)
      strictEntityFuture.flatMap { strictEntity =>
        val guitarJsonString = strictEntity.data.utf8String
        val guitar = guitarJsonString.parseJson.convertTo[Guitar]

        val guitarCreatedFuture: Future[GuitarCreated] = (guitarDb ? CreateGuitar(guitar)).mapTo[GuitarCreated]
        guitarCreatedFuture.map { _ =>
          HttpResponse(StatusCodes.OK)
        }
      }

    case HttpRequest(HttpMethods.POST, uri@Uri.Path("/api/guitar/inventory"), _, _, _) =>
      val query = uri.query()
      val guitarId: Option[Int] = query.get("id").map(_.toInt)
      val guitarQuantity: Option[Int] = query.get("quantity").map(_.toInt)

      val validGuitarResponseFuture: Option[Future[HttpResponse]] = for {
        id <- guitarId
        quantity <- guitarQuantity
      } yield {
        val newGuitarFuture: Future[Option[Guitar]] = (guitarDb ? AddQuantity(id, quantity)).mapTo[Option[Guitar]]
        newGuitarFuture.map(_ => HttpResponse(StatusCodes.OK))
      }
      validGuitarResponseFuture.getOrElse(Future(HttpResponse(StatusCodes.BadRequest)))

    case HttpRequest(HttpMethods.GET, uri@Uri.Path("/api/guitar/inventory"), _, _, _) =>
      val query = uri.query()
      val inStockOption = query.get("inStock").map(_.toBoolean)

      inStockOption match {
        case Some(inStock) =>
          val guitarsFuture: Future[List[Guitar]] = (guitarDb ? FindGuitarsInStock(inStock)).mapTo[List[Guitar]]
          guitarsFuture.map { guitars =>
            HttpResponse(
              entity = HttpEntity(
                ContentTypes.`application/json`,
                guitars.toJson.prettyPrint
              )
            )
          }
        case None =>
          Future(HttpResponse(StatusCodes.BadRequest))
      }

    case request: HttpRequest =>
      request.discardEntityBytes()
      Future {
        HttpResponse(status = StatusCodes.NotFound)
      }
  }

  Http()
    .newServerAt("localhost", 8080)
    .bind(requestHandler)

  /*
   * Exercise: enhance the Guitar case class with a quantity field (items in stock),
   *           by default 0
   *
   *  - GET   /api/guitar/inventory?inStock=true/false  which returns the guitars in stock as a JSON
   *  - POST /api/guitar/inventory?id=X&quantity=Y      which adds Y guitars to the stock for guitar with id X
   */
}
