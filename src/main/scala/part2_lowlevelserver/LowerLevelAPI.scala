package part2_lowlevelserver

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.IncomingConnection
import akka.http.scaladsl.model.headers.Location
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, HttpResponse, StatusCodes, Uri}
import akka.stream.scaladsl.{Flow, Sink}

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}

object LowerLevelAPI extends App {
  /*
   * Goal
   *  Setup our first Akka HTTP server
   *  Understand the basic principles of Akka HTTP
   *
   * Assumed as known:
   *  - Akka, Akka Streams and advanced Scala
   *  - HTTP basics: requests, responses, statuses, headers
   *  - JSON: syntax, structure
   *
   * Akka HTTP basics
   *  Akka HTTP is
   *    - an un-opinionated suite of libraries
   *    - focused on HTTP integration of an application
   *    - designed for both servers and clients
   *    - based on Akka actors and Akka Streams
   *
   *  Akka HTTP is NOT:
   *    - a framework
   *
   *  Akka HTTP strengths
   *    - stream-based, with backpressure for free
   *      goes down well all the way to TCP level
   *    - multiple API levels for control vs ease of use
   *
   *  Core concepts (few listed here)
   *    - HttpRequest, HttpResponse
   *    - HttpEntity
   *      - payload for both requests and responses
   *    - marshalling
   *      - process of turning your own data, the data that application understands
   *        into a so called wire format such as JSON
   *
   *  Akka HTTP server
   *    Goal: receive HTTP request, send HTTP responses
   *      - synchronously via a function HttpRequest => HttpResponse
   *      - async via a function HttpRequest => Future[HttpResponse]
   *      - async via streams, with a Flow[HttpRequest, HttpResponse, _]
   *    (all of the above turn into flows sooner or later)
   *
   *  Under the hood:
   *    - the server receives HttpRequests (transparently)
   *    - the requests go through the flow we write
   *    - the resulting responses are served back (transparently)
   */

  implicit private val system: ActorSystem = ActorSystem("LowLevelServerAPI")

  import system.dispatcher // ExecutionContext for futures - use your own

  // source of connections
  private val serverSource = Http().newServerAt("localhost", 8000).connectionSource()
  private val connectionSink = Sink.foreach[IncomingConnection] { connection =>
    println(s"Accepted incoming connection from: ${connection.remoteAddress}")
  }

  private val serverBindingFuture = serverSource.to(connectionSink).run()
  serverBindingFuture.onComplete {
    case Success(binding) =>
      println("Server binding successful")
      // binding.unbind()
      binding.terminate(2.seconds)
    case Failure(exception) => println(s"Server binding failed: $exception")
  }

  /*
   * Method 1: synchronously server HTTP responses
   */
  private val requestHandler: HttpRequest => HttpResponse = {
    case HttpRequest(HttpMethods.GET, uri, value, entity, protocol) =>
      HttpResponse(
        StatusCodes.OK, // HTTP 200
        entity = HttpEntity(
          ContentTypes.`text/html(UTF-8)`,
          """
            |<html>
            | <body>
            |   Hello from Akka HTTP!
            | </body>
            |</html>
            |""".stripMargin
        )
      )

    case request: HttpRequest =>
      request.discardEntityBytes()
      HttpResponse(
        StatusCodes.NotFound, // 404
        entity = HttpEntity(
          ContentTypes.`text/html(UTF-8)`,
          """
            |<html>
            | <body>
            |   OOPS! The resource can't be found.
            | </body>
            |</html>
            |""".stripMargin
        )
      )
  }

  private val httpSyncConnectionHandler = Sink.foreach[IncomingConnection] { connection =>
    connection.handleWithSyncHandler(requestHandler)
  }

  /*Http()
    .newServerAt("localhost", 8080)
    .connectionSource()
    .runWith(httpSyncConnectionHandler)*/

  // shorthand version
  /*Http()
    .newServerAt("localhost", 8080)
    .bindSync(requestHandler)*/

  /*
   * Method 2: serve back HTTP response ASYNCHRONOUSLY
   */
  private val asyncRequestHandler: HttpRequest => Future[HttpResponse] = {
    case HttpRequest(HttpMethods.GET, Uri.Path("/home"), value, entity, protocol) =>
      Future(HttpResponse(
        StatusCodes.OK, // HTTP 200
        entity = HttpEntity(
          ContentTypes.`text/html(UTF-8)`,
          """
            |<html>
            | <body>
            |   Hello from Akka HTTP!
            | </body>
            |</html>
            |""".stripMargin
        )
      ))

    case request: HttpRequest =>
      request.discardEntityBytes()
      Future(HttpResponse(
        StatusCodes.NotFound, // 404
        entity = HttpEntity(
          ContentTypes.`text/html(UTF-8)`,
          """
            |<html>
            | <body>
            |   OOPS! The resource can't be found.
            | </body>
            |</html>
            |""".stripMargin
        )
      ))
  }

  // useful when it takes time in the server computation/rendering
  private val httpAsyncConnectionHandler = Sink.foreach[IncomingConnection] { connection =>
    connection.handleWithAsyncHandler(asyncRequestHandler)
  }

  // streams-based "manual" version
  /*Http()
    .newServerAt("localhost", 8081)
    .connectionSource()
    .runWith(httpAsyncConnectionHandler)*/

  // shorthand version
  /*Http()
    .newServerAt("localhost", 8081)
    .bind(asyncRequestHandler)*/

  /*
   * Method 3: async via Akka streams
   */
  private val streamBasedRequestHandler: Flow[HttpRequest, HttpResponse, _] = Flow[HttpRequest].map {
    case HttpRequest(HttpMethods.GET, Uri.Path("/home"), value, entity, protocol) =>
      HttpResponse(
        StatusCodes.OK, // HTTP 200
        entity = HttpEntity(
          ContentTypes.`text/html(UTF-8)`,
          """
            |<html>
            | <body>
            |   Hello from Akka HTTP!
            | </body>
            |</html>
            |""".stripMargin
        )
      )

    case request: HttpRequest =>
      request.discardEntityBytes()
      HttpResponse(
        StatusCodes.NotFound, // 404
        entity = HttpEntity(
          ContentTypes.`text/html(UTF-8)`,
          """
            |<html>
            | <body>
            |   OOPS! The resource can't be found.
            | </body>
            |</html>
            |""".stripMargin
        )
      )
  }

  // "manual" version
  /*Http()
    .newServerAt("localhost", 8082)
    .connectionSource()
    .runForeach { connection =>
      connection.handleWith(streamBasedRequestHandler)
    }*/

  // shorthand version
  /*Http()
    .newServerAt("localhost", 8082)
    .bindFlow(streamBasedRequestHandler)*/

  /*
   * Exercise: create your own HTTP server running on localhost on 8388, which replies
   *  - with a welcome message on the "front door" localhost:8388
   *  - with a proper HTML on localhost:8388/about
   *  - with a 404 message otherwise
   */
  private val syncExerciseHandler: HttpRequest => HttpResponse = {
    case HttpRequest(HttpMethods.GET, Uri.Path("/"), value, entity, protocol) =>
      HttpResponse(
        // status code OK (200) is default
        entity = HttpEntity(
          ContentTypes.`text/html(UTF-8)`,
          "Hello from the exercise from door!"
        )
      )
    case HttpRequest(HttpMethods.GET, Uri.Path("/about"), value, entity, protocol) =>
      HttpResponse(
        // status code OK (200) is default
        entity = HttpEntity(
          ContentTypes.`text/html(UTF-8)`,
          """
            |<html>
            | <body>
            |   <div style="color:red;">
            |     Hello from the about page!
            |   </div>
            | </body>
            |</html>
            |""".stripMargin
        )
      )
    // path /search redirects to some other part of our website/webapp/microservice
    case HttpRequest(HttpMethods.GET, Uri.Path("/search"), value, entity, protocol) =>
      HttpResponse(
        StatusCodes.Found,
        headers = List(Location("https://google.com"))
      )
    case request: HttpRequest =>
      request.discardEntityBytes()
      HttpResponse(
        StatusCodes.NotFound, // 404
        entity = HttpEntity(
          ContentTypes.`text/html(UTF-8)`,
          "OOPS, you're in no man's land, sorry"
        )
      )
  }

  private val bindingFuture = Http()
    .newServerAt("localhost", 8388)
    .bindSync(syncExerciseHandler)

  // shutdown the server:
  bindingFuture
    .flatMap(binding => binding.unbind())
    .onComplete(_ => system.terminate())
}
